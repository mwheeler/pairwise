FROM node:10 AS build
RUN mkdir app
WORKDIR /app
COPY . .
RUN npm install && npm run build

FROM nginx:1.16.0
RUN mkdir /app
RUN chmod g+rwx /var/cache/nginx /var/run /var/log/nginx
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build /app/dist /app
