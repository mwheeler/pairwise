import Vue from 'vue'
import Router from 'vue-router'
import VotePage from '@/components/VotePage'
import CandidatesPage from '@/components/CandidatesPage'
import ResultsPage from '@/components/ResultsPage'
import store from '@/store'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'VotePage',
      component: VotePage
    },
    {
      path: '/candidates',
      name: 'CandidatesPage',
      component: CandidatesPage
    },
    {
      path: '/results',
      name: 'ResultsPage',
      component: ResultsPage
    }
  ]
})
