import Vue from 'vue';
import Vuex from 'vuex';

import _ from 'lodash';

import { nextBallot } from '@/lib/pairwise';
import { getRanking, updateVotes } from '../lib/pairwise';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    candidates: [
      /*
      'six',
      'one',
      'nine',
      'five'
      */
    ],
    votes: []
  },
  getters: {
    ranked: state => {
      return getRanking(state.candidates, state.votes);
    },
    unranked: state => {
      return state.candidates.filter(candidate => !candidate.rank);
    },
    nextBallot: state => {
      return nextBallot(state.candidates, state.votes);
    }
  },
  mutations: {
    add (state, payload) {
      state.candidates.push(payload);
    },
    recordVote (state, payload) {
      state.votes = updateVotes(payload, state.votes);
      // state.votes.push(payload);
    }
  }
});

export default store;
