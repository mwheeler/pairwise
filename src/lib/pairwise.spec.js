import {
  pairsEqual,
  possiblePairs,
  latestVotes,
  remainingPairs,
  pointsEarned,
  pointsOffered,
  updateVotes,
  nextBallot,
  minNewVotes,
  getRanking
} from '@/lib/pairwise';

describe('pairwise.js', () => {
  describe('#pairsEqual', () => {
    it('should return true for equal ballots', () => {
      let a = ['one', 'two'];
      let b = ['one', 'two'];
      expect(pairsEqual(a, b)).toBe(true);
    });
    it('should return true for ballots with the same candidates', () => {
      let a = ['one', 'two'];
      let b = ['two', 'one'];
      expect(pairsEqual(a, b)).toBe(true);
    });
    it('should return false for pairs with different candidates', () => {
      let a = ['one', 'two'];
      let b = ['one', 'too'];
      expect(pairsEqual(a, b)).toBe(false);
    });
    it('should not mutate arguments', () => {
      let a = ['one', 'two'];
      let b = ['two', 'one'];
      pairsEqual(a, b);
      expect(a).toEqual(['one', 'two']);
      expect(b).toEqual(['two', 'one']);
    });
  });

  describe('#possiblePairs', () => {
    it('should return empty list for one candidate', () => {
      expect(possiblePairs(['one'])).toHaveLength(0);
    });
    it('should return candidates for length two', () => {
      let candidates = ['one', 'two'];
      expect(possiblePairs(candidates)).toHaveLength(1);
    });
    it('should return all possible ballots', () => {
      let candidates = ['one', 'two', 'three', 'four'];
      expect(possiblePairs(candidates)).toHaveLength(6);
      let sixCandidates = [
        'one', 'two', 'three', 'four', 'five', 'six'
      ];
      expect(possiblePairs(sixCandidates)).toHaveLength(15);
    });
    it('should not mutate the list of candidates', () => {
      let sixCandidates = [
        'one', 'two', 'three', 'four', 'five', 'six'
      ];
      possiblePairs(sixCandidates);
      expect(sixCandidates).toHaveLength(6);
    });
  });

  describe('#latestVotes', () => {
    it('should return the most recent results for each pair', () => {
      let all = [
        ['one', 'three'],  // oldest, should be removed
        ['one', 'two'],
        ['three', 'two'],  // should be removed
        ['one', 'three'],
        ['two', 'three']  // newest vote
      ];
      let latest = [
        ['one', 'two'],  // oldest
        ['one', 'three'],
        ['two', 'three']  // newest vote
      ];
      expect(latestVotes(all)).toEqual(latest);
    });
    it('should not mutate argument', () => {
      let all = [
        ['one', 'three'],
        ['one', 'two'],
        ['three', 'two'],
        ['one', 'three'],
        ['two', 'three']
      ];
      latestVotes(all);
      expect(all).toEqual([
        ['one', 'three'],
        ['one', 'two'],
        ['three', 'two'],
        ['one', 'three'],
        ['two', 'three']
      ]);
    });
  });

  describe('#remainingPairs', () => {
    it('should find missing votes', () => {
      let candidates = [
        'one', 'two', 'three', 'four'
      ];
      let votes = [
        ['one', 'two'],                   ['one', 'four'],
                        ['two', 'three'],
                                          ['three', 'four']
      ];  // missing one-three and two-four
      let remaining = remainingPairs(candidates, votes);
      expect(remaining).toHaveLength(2);
      expect(remaining).toContainEqual(['one', 'three']);
      expect(remaining).toContainEqual(['two', 'four']);
    });
    it('should not mutate arguments', () => {
      let candidates = ['one', 'two', 'three'];
      let votes = [['one', 'two'], ['one', 'three']];
      remainingPairs(candidates, votes);
      expect(candidates).toEqual(['one', 'two', 'three']);
      expect(votes).toEqual([
        ['one', 'two'],
        ['one', 'three']
      ]);
    });
  });

  describe('#pointsEarned', () => {
    it('should return one point for each winning vote', () => {
      let votes = [
        ['three', 'two'],
        ['one', 'two'],
        ['one', 'three'],
        ['two', 'three']
      ];
      expect(pointsEarned('one', votes)).toBe(2);
      expect(pointsEarned('two', votes)).toBe(1);
      expect(pointsEarned('three', votes)).toBe(0);
    });
  });

  describe('#pointsOffered',() => {
    it('should return one point for each participant', () => {
      let votes = [
        ['three', 'two'],
        ['one', 'two'],
        ['one', 'three'],
        ['two', 'three'],
        ['four', 'three']
      ];
      expect(pointsOffered('one', votes)).toBe(2);
      expect(pointsOffered('two', votes)).toBe(2);
      expect(pointsOffered('three', votes)).toBe(3);
      expect(pointsOffered('four', votes)).toBe(1);
    });
  });

  describe('#updateVotes', () => {
    it('should update votes', () => {
      let vote = ['2', '3'];
      let oldVotes = [
        ['1', '2'],
        ['3', '4']
      ];
      expect(updateVotes(vote, oldVotes)).toEqual([
        ['1', '2'],
        ['3', '4'],
        ['2', '3'],
        ['1', '3'],
        ['2', '4']
      ]);
    });
  });

  describe('#minNewVotes', () => {
    it('should return the minimum number of votes created by a new ballot', () => {
      let votes = [
        ['1', '2'],
        ['3', '4'],
        ['1', '4']
      ];
      let ballotA = ['2', '3'];
      expect(minNewVotes(ballotA, votes)).toBe(4)
    });
  });

  describe('#nextBallot', () => {
    it('should select the best ballot', () => {
      let oldVotes = [
        ['1', '2'],
        ['3', '4']
      ];
      let next = nextBallot(['1', '2', '3', '4'], oldVotes);
      expect(next).toEqual(['2', '4']);
    });
  });

  describe('#getRanking', () => {
    it('should return the correct ranking', () => {
      let candidates = ['2', '1', '4', '3'];
      let votes = [
        ['2', '3'],
        ['1', '2'],
        ['1', '4'],
        ['2', '4'],
        ['1', '3'],
        ['3', '4']
      ];
      let ranking = getRanking(candidates, votes);
      expect(ranking).toEqual(['1', '2', '3', '4']);
    })
  })
});
