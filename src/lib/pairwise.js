import _ from 'lodash';

export function pairsEqual(a, b) {
  if (_.isEqual(a, b)) {
    return true;
  }
  return _.isEqual(a, _.clone(b).reverse());
}

export function possiblePairs(candidates, pairs=[]) {
  if (candidates.length < 2) {
    return pairs;
  }
  if (candidates.length === 2) {
    pairs.push(candidates);
    return pairs;
  }
  let first = _.head(candidates);
  let remainder = _.tail(candidates);
  let newPairs = pairs.concat(remainder.map(
    candidate => [first, candidate]
  ));

  return possiblePairs(remainder, newPairs);
}

export function latestVotes(allVotes) {
  allVotes = _.clone(allVotes).reverse();
  return _.uniqWith(allVotes, pairsEqual).reverse();
}

export function remainingPairs(candidates, allVotes) {
  let needed = possiblePairs(candidates);
  return _.differenceWith(needed, allVotes, pairsEqual);
}

export function pointsEarned(candidate, allVotes) {
  let votes = latestVotes(allVotes);
  let winners = votes.map(v => v[0]);
  return winners.filter(winner => winner === candidate).length;
}

export function pointsOffered(candidate, allVotes) {
  let votes = latestVotes(allVotes);
  let participants = _.flatten(votes);
  return participants.filter(
    participant => participant === candidate
  ).length;
}

export function updateVotes(ballot, votes) {
  let [winner, loser] = ballot;
  let impliedWins = votes.filter(
    v => v[1] === winner
  ).map(
    v => [v[0], loser]
  );
  let impliedLosses = votes.filter(
    v => v[0] === loser
  ).map(
    v => [winner, v[1]]
  );
  return _.uniqWith(
    votes.concat(
      [ballot],
      impliedWins,
      impliedLosses
    ), pairsEqual
  );
}

export function minNewVotes(ballot, votes) {
  let [a, b] = ballot;
  return Math.min(
    updateVotes(ballot, votes).length,
    updateVotes([b, a], votes).length
  );
}

export function maxNewVotes(ballot, votes) {
  let [a, b] = ballot;
  return Math.max(
    updateVotes([a, b], votes).length,
    updateVotes([b, a], votes).length
  );
}

export function nextBallot(candidates, votes) {
  let remaining = remainingPairs(candidates, votes);
  let minVotes = _.curryRight(minNewVotes)(votes);
  let maxVotes = _.curryRight(maxNewVotes)(votes);
  let options = _.sortBy(remaining, [minVotes, maxVotes]);
  return _.takeRight(options)[0];
}

export function getWins(votes, candidate) {
  return votes.map(
    vote => vote[0]
  ).filter(
    name => name === candidate
  ).length;
}

export function getRanking(candidates, votes) {
  let ranking = _.sortBy(
      [...candidates],
      [_.curry(getWins)(votes)]
  ).reverse();
  return ranking;
}
